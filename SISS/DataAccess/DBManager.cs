﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class DBManager
    {
        private SqlConnection Conexion;
        private String CadenaConexion;

        public DBManager()
        {
            CadenaConexion = Properties.Settings.Default.ConectionString;
            this.Conexion = new SqlConnection(CadenaConexion);
        }

        public SqlConnection GetConnection()
        {
            return Conexion;
        }

        public void CerrarConexion()
        {
            if (Conexion != null)
            {
                if (Conexion.State == System.Data.ConnectionState.Open)
                {
                    Conexion.Close();
                }
            }
        }
    }
}

