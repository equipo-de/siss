﻿using System;
using BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessLogicTest
{
    [TestClass]
    public class OrganizacionDAOTest
    {
        [TestMethod]
        public void CrearOrganizacionTest()
        {
            OrganizacionDAO organizacionDAO = new OrganizacionDAO();
            Organizacion organizacionSS = new Organizacion();
            organizacionSS.Nombre = "Xalasoft";
            organizacionSS.Sector = "Desarrollo de software";
            organizacionSS.Correo = "Eyizon@hotmail.com";
            organizacionSS.Ciudad = "Xalapa";
            organizacionSS.Direccion = "Cordoba No.17";
            organizacionSS.Telefono = "9211235290";
            Assert.AreEqual(true, organizacionDAO.CrearOrganizacion(organizacionSS));
        }

        [TestMethod]
        public void CrearProyectoSinSectorTelefonoTest()
        {
            OrganizacionDAO organizacionDAO = new OrganizacionDAO();
            Organizacion organizacionSS = new Organizacion();
            organizacionSS.Nombre = "Xalasoft";
            organizacionSS.Correo = "Eyizon@hotmail.com";
            organizacionSS.Ciudad = "Xalapa";
            organizacionSS.Direccion = "Cordoba No.17";
            Assert.AreEqual(true, organizacionDAO.CrearOrganizacion(organizacionSS));
        }

        [TestMethod]
        public void CrearProyectoSoloConNombreTest()
        {
            OrganizacionDAO organizacionDAO = new OrganizacionDAO();
            Organizacion organizacionSS = new Organizacion();
            organizacionSS.Nombre = "Xalasoft";

            Assert.AreEqual(true, organizacionDAO.CrearOrganizacion(organizacionSS));
        }
    }
}
