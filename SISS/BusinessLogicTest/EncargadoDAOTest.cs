﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLogic;

namespace BusinessLogicTest
{
    [TestClass]
    public class EncargadoDAOTest
    {
        [TestMethod]
        public void CrearEncargadoTest()
        {
            EncargadoDAO encargadoDAO = new EncargadoDAO();
            Encargado encargadoSS = new Encargado();
            encargadoSS.Nombre = "Xalasoft";
            encargadoSS.Apellidos = "Desarrollo de software";
            encargadoSS.Correo = "Eyizon@hotmail.com";
            encargadoSS.Password = "Xalapa";
            encargadoSS.Telefono = "9211235290";
            encargadoSS.Direccion = "Cordoba No.17";
            Assert.AreEqual(true, encargadoDAO.CrearEncargado(encargadoSS));
        }

        [TestMethod]
        public void CrearEncargadoSinTelefonoTest()
        {
            EncargadoDAO encargadoDAO = new EncargadoDAO();
            Encargado encargadoSS = new Encargado();
            encargadoSS.Nombre = "Xalasoft";
            encargadoSS.Apellidos = "Desarrollo de software";
            encargadoSS.Correo = "Eyizon@hotmail.com";
            encargadoSS.Password = "Xalapa";
            encargadoSS.Direccion = "Cordoba No.17";
            Assert.AreEqual(true, encargadoDAO.CrearEncargado(encargadoSS));
        }

        [TestMethod]
        public void CrearEncargadoSoloConNombreTest()
        {
            EncargadoDAO encargadoDAO = new EncargadoDAO();
            Encargado encargadoSS = new Encargado();
            encargadoSS.Nombre = "Xalasoft";
            encargadoSS.Apellidos = "Desarrollo de software";
            Assert.AreEqual(true, encargadoDAO.CrearEncargado(encargadoSS));
        }
    }
}
