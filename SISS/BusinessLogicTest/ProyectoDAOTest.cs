﻿using System;
using BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BusinessLogicTest
{
    [TestClass]
    public class ProyectoDAOTest
    {
      
        [TestMethod]
        public void CrearProyectoTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Proyecto proyecto = new Proyecto();
            DateTime dateTime = new DateTime(2019, 6, 1);
            proyecto.Nombre = "Asistente de proyecto de software";
            proyecto.Descripcion = "Asistencia en el desarrollo de proyectos de software";
            proyecto.NumeroAlumnosMax = 3;
            proyecto.FechaInicio = dateTime;
            proyecto.Estado = "visible";
            Assert.AreEqual(true, proyectoDAO.CrearProyecto(proyecto));
        }

        [TestMethod]
        public void CrearProyectoSinFechaInicioTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Proyecto proyecto = new Proyecto();
            proyecto.Nombre = "Asistente de proyecto de software";
            proyecto.Descripcion = "Asistencia en el desarrollo de proyectos de software";
            proyecto.NumeroAlumnosMax = 3;
            proyecto.Estado = "visible";
            Assert.AreEqual(true, proyectoDAO.CrearProyecto(proyecto));
        }

        [TestMethod]
        public void CrearProyectoSinNombreTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Proyecto proyecto = new Proyecto();
            DateTime dateTime = new DateTime(2019, 6, 1);
            proyecto.Descripcion = "Asistencia en el desarrollo de proyectos de software";
            proyecto.NumeroAlumnosMax = 3;
            proyecto.FechaInicio = dateTime;
            proyecto.Estado = "visible";
            Assert.AreEqual(false, proyectoDAO.CrearProyecto(proyecto));
        }

        [TestMethod]
        public void CrearProyectoSinValorTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Proyecto proyecto = new Proyecto();
            Assert.AreEqual(false, proyectoDAO.CrearProyecto(proyecto));
        }

        [TestMethod]
        public void EditarProyectoTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Assert.AreEqual(1, proyectoDAO.EditarProyecto(1));
        }
        
        [TestMethod]
        public void EditarProyectoInexistenteTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Assert.AreEqual(0, proyectoDAO.EditarProyecto(20));
        }

        [TestMethod]
        public void OcultarProyectoTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Assert.AreEqual(true, proyectoDAO.OcultarProyecto(1));
        }

        [TestMethod]
        public void VisibilizarProyectoTest()
        {
            ProyectoDAO proyectoDAO = new ProyectoDAO();
            Assert.AreEqual(true, proyectoDAO.VisibilizarProyecto(1));
        }
    }
}
    

