﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    interface IEncargadoDAO
    {
        bool CrearEncargado(Encargado encargado);
        int EditarEncargado(int idEncargado);
        void OcultarEncargado(Encargado encargadoSS);
    }
}
