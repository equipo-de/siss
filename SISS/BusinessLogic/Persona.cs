﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Persona
    {
        private String nombre;
        public String Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        public String Apellidos { get; set; }
        public String Correo { get; set; }
        public String Password { get; set; }
        public String Direccion { get; set; }
        public String Telefono { get; set; }
        public EGenero Genero { get; set; }

        public enum EGenero
        {
            Masculino,
            Femenino
        }

        
    }
}
