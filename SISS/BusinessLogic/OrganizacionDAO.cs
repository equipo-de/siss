﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace BusinessLogic
{
    public class OrganizacionDAO : IOrganizacionDAO
    {
        public bool CrearOrganizacion(Organizacion organizacion)
        {
            DBManager dbManager = new DBManager();
            using (SqlConnection conexion = dbManager.GetConnection())
            {
                try
                {
                    conexion.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO organizacion (nombre_org, sector_org, correo_org, ciudad_org,direccion_org,telefono_org ) VALUES (@nombreorg, @sector, @correoorg, @ciudadorg,@direccionorg,@telefonoorg);", conexion))
                    {
                        command.Parameters.Add(new SqlParameter("@nombreorg", System.Data.SqlDbType.VarChar, 80));
                        command.Parameters["@nombreorg"].Value = organizacion.Nombre;
                        command.Parameters.Add(new SqlParameter("@sector", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@sector"].Value = organizacion.Sector;
                        command.Parameters.Add(new SqlParameter("@correoorg", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@correoorg"].Value = organizacion.Correo;
                        command.Parameters.Add(new SqlParameter("@ciudadorg", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@ciudadorg"].Value = organizacion.Ciudad;
                        command.Parameters.Add(new SqlParameter("@direccionorg", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@direccionorg"].Value = organizacion.Direccion;
                        command.Parameters.Add(new SqlParameter("@telefonoorg", System.Data.SqlDbType.VarChar, 18));
                        command.Parameters["@telefonoorg"].Value = organizacion.Telefono;

                        command.ExecuteNonQuery();
                    }
                    dbManager.CerrarConexion();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Errors);
                }
               
            }
            return true;
        }

        public String EditarOrganizacion(string idOrganizacion)
        {
            DBManager dbManager = new DBManager();

            using (SqlConnection conexion = dbManager.GetConnection())
            {
                try
                {
                    Organizacion organizacion = new Organizacion();
                    conexion.Open();
                    using (SqlCommand command = new SqlCommand("SELECT id_org, nombre_org, sector_org, correo_org, ciudad_org,direccion_org,telefono_org, FROM organizacion WHERE id_org=@id_Organizacion;", conexion))
                    {
                        command.Parameters.AddWithValue("@id_Organizacion", idOrganizacion);

                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            organizacion.Id = reader["id_org"].ToString();
                            organizacion.Nombre = reader["nombre_proyecto"].ToString();
                            organizacion.Sector = reader["sector_org"].ToString();
                            organizacion.Correo = reader["correo_org"].ToString();
                            organizacion.Ciudad = reader["ciudad_org"].ToString();
                            organizacion.Direccion = reader["direccion_org"].ToString();
                            organizacion.Telefono = reader["telefono_org"].ToString();
                        }
                    }
                    dbManager.CerrarConexion();
                    return organizacion.Id;
                }
                catch (SqlException exception)
                {
                    Console.WriteLine(exception.Errors);
                    return null;
                }
            }
        }

        public bool OcultarOrganizacion(String idOrganizacion)
        {
            throw new NotImplementedException();
        }
    }
}

