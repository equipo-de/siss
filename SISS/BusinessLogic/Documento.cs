﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class Documento
    {
        private int idDocumento;
        public int IdDocumento
        {
            get
            {
                return idDocumento;
            }
            set
            {
                idDocumento = value;
            }
        }
        private DateTime fechaCreacion;
        public DateTime FechaCreacion
        {
            get
            {
                return fechaCreacion;
            }
            set
            {
                fechaCreacion = value;
            }
        }
        private DateTime fechaActualizacion;
        public DateTime FechaActualizacion
        {
            get
            {
                return fechaActualizacion;
            }
            set
            {
                fechaActualizacion = value;
            }
        }
        private ETipoDocumento tipoDocumento;
        public ETipoDocumento TipoDocumento
        {
            get
            {
                return tipoDocumento;
            }
            set
            {
                tipoDocumento = value;
            }
        }

        public enum ETipoDocumento
        {
            ReporteMensual,
            CartaAceptacion,
            CartaPresentacion,
            Memoria,
            CartaLIberacion
        }

        public List<Documento> ConsultarDocumentos()
        {
            List<Documento> ListaDocumentos = new List<Documento>();
            //todo
            return ListaDocumentos;
        }
    }
}
