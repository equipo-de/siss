﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    interface IProyectoDAO
    {
        bool CrearProyecto(Proyecto proyectoSS);
        int EditarProyecto(int idProyecto);
        bool OcultarProyecto(int idProyecto);
    }
}
