﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Organizacion
    {
        private String id;
        public String Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        private String nombre;
        public String Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }
        private String sector;
        public String Sector
        {
            get
            {
                return sector;
            }
            set
            {
                sector = value;
            }
        }
        private String correo;
        public String Correo
        {
            get
            {
                return correo;
            }
            set
            {
                correo = value;
            }
        }
        private String ciudad;
        public String Ciudad
        {
            get
            {
                return ciudad;
            }
            set
            {
                ciudad = value;
            }
        }
        private String telefono;
        public String Telefono
        {
            get
            {
                return telefono;
            }
            set
            {
                telefono = value;
            }
        }
        private String direccion;
        public String Direccion
        {
            get
            {
                return direccion;
            }
            set
            {
                direccion = value;
            }
        }
        private Coordinador vinculadoPor;
        public Coordinador VinculadoPor
        {
            get
            {
                return vinculadoPor;
            }
            set
            {
                vinculadoPor = value;
            }
        }
        List<Encargado> ListaEncargados;
    }
}
