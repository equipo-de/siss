﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;
using System.Data.SqlClient;

namespace BusinessLogic
{
    public class EncargadoDAO : IEncargadoDAO
    {
        public bool CrearEncargado(Encargado encargado)
        {
            DBManager dbManager = new DBManager();
            using (SqlConnection conexion = dbManager.GetConnection())
            {
                try
                {
                    conexion.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO encargado (nombre_encargado, apellido_encargado, correo_encargado,password_encargado,direccion_encargado,telefono_encargado) VALUES (@nombreen, @apellidoen, @correoen, @passworden,@direccionen,@telefonoen);", conexion))
                    {
                        command.Parameters.Add(new SqlParameter("@nombreen", System.Data.SqlDbType.VarChar, 30));
                        command.Parameters["@nombreen"].Value = encargado.Nombre;
                        command.Parameters.Add(new SqlParameter("@apellidoen", System.Data.SqlDbType.VarChar, 30));
                        command.Parameters["@apellidoen"].Value =encargado.Apellidos;
                        command.Parameters.Add(new SqlParameter("@correoen", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@correoen"].Value = encargado.Correo;
                        command.Parameters.Add(new SqlParameter("@passworden", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@passworden"].Value = encargado.Password;
                        command.Parameters.Add(new SqlParameter("@direccionen", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@direccionen"].Value = encargado.Direccion;
                        command.Parameters.Add(new SqlParameter("@telefonoen", System.Data.SqlDbType.VarChar, 18));
                        command.Parameters["@telefonoen"].Value = encargado.Telefono;

                        command.ExecuteNonQuery();
                    }
                    dbManager.CerrarConexion();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Errors);
                }

            }
            return true;
        }

        public int EditarEncargado(int idEncargado)
        {
            throw new NotImplementedException();
        }

        public void OcultarEncargado(Encargado encargado)
        {
            throw new NotImplementedException();
        }
    }
}
