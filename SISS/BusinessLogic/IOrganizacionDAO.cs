﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    interface IOrganizacionDAO
    {
        bool CrearOrganizacion(Organizacion organizacion);
        String EditarOrganizacion(string idOrganizacion);
        bool OcultarOrganizacion(String idOrganizacion);
    }
}
