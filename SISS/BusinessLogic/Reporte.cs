﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class Reporte
    {
        private String actividades;
        public String Actividades
        {
            get
            {
                return actividades;
            }
            set
            {
                actividades = value;
            }
        }
        private int horas;
        public int Horas
        {
            get
            {
                return horas;
            }
            set
            {
                horas = value;
            }
        }
    }
}
