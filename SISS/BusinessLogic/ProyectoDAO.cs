﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class ProyectoDAO : IProyectoDAO
    {
        public bool CrearProyecto(Proyecto Proyecto)
        {
            DBManager dbManager = new DBManager();
            using (SqlConnection conexion = dbManager.GetConnection())
            {
                try
                {
                    conexion.Open();
                    using (SqlCommand command = new SqlCommand("INSERT INTO proyecto (nombre_proyecto, descripcion_proyecto, numero_alumnos_max, fecha_inicio) VALUES (@nombre, @descripcion, @numero_alumnos, @fecha_inicio);", conexion))
                    {
                        command.Parameters.Add(new SqlParameter("@nombre", System.Data.SqlDbType.VarChar, 60));
                        command.Parameters["@nombre"].Value = Proyecto.Nombre;
                        command.Parameters.Add(new SqlParameter("@descripcion", System.Data.SqlDbType.VarChar, 255));
                        command.Parameters["@descripcion"].Value = Proyecto.Descripcion;
                        command.Parameters.Add(new SqlParameter("@numero_alumnos", System.Data.SqlDbType.Int, 4));
                        command.Parameters["@numero_alumnos"].Value = Proyecto.NumeroAlumnosMax;
                        command.Parameters.Add(new SqlParameter("@fecha_inicio", System.Data.SqlDbType.Date));
                        command.Parameters["@fecha_inicio"].Value = Proyecto.FechaInicio;

                        command.ExecuteNonQuery();
                    }
                    dbManager.CerrarConexion();
                }
                catch (SqlException ex)
                {
                    Console.WriteLine(ex.Errors);
                    return false;
                }
            }
            return true;
        }

        public int EditarProyecto(int idProyecto)
        {
            DBManager dbManager = new DBManager();

            using (SqlConnection conexion = dbManager.GetConnection())
            {
                try
                {
                    Proyecto proyecto = new Proyecto();
                    conexion.Open();
                    using (SqlCommand command = new SqlCommand("SELECT id_proyecto, nombre_proyecto, descripcion_proyecto, numero_alumnos_max, fecha_inicio FROM proyecto WHERE id_proyecto=@id_proyecto;", conexion))
                    {
                        command.Parameters.AddWithValue("@id_proyecto", idProyecto);
                        
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            proyecto.Id = (int)reader["id_proyecto"];
                            proyecto.Nombre = reader["nombre_proyecto"].ToString();
                            proyecto.Descripcion = reader["descripcion_proyecto"].ToString();
                            proyecto.NumeroAlumnosMax = (int)reader["numero_alumnos_max"];
                        }
                    }
                    dbManager.CerrarConexion();
                    return proyecto.Id;
                }
                catch (SqlException exception)
                {
                    Console.WriteLine(exception.Errors);
                    return 0;
                }
            }

        }

        public bool OcultarProyecto(int idProyecto)
        {
            DBManager dbManager = new DBManager();
            using (SqlConnection conexion = dbManager.GetConnection())
            {
                try
                {
                    conexion.Open();
                    using (SqlCommand command = new SqlCommand("UPDATE proyecto SET estado_proyecto='oculto' WHERE id_proyecto=@id_proyecto;"))
                    {
                        command.Parameters.AddWithValue("@id_proyecto", idProyecto);
                        command.ExecuteNonQuery();
                    }
                    dbManager.CerrarConexion();
                }
                catch (SqlException exception)
                {
                    Console.WriteLine(exception.Errors);
                    return false;
                }
            }
            return true;
        }

        public bool VisibilizarProyecto(int idProyecto)
        {
            DBManager dbManager = new DBManager();
            using (SqlConnection conexion = dbManager.GetConnection())
            {
                try
                {
                    conexion.Open();
                    using (SqlCommand command = new SqlCommand("UPDATE proyecto SET estado_proyecto='visible' WHERE id_proyecto=@id_proyecto;"))
                    {
                        command.Parameters.AddWithValue("@id_proyecto", idProyecto);
                        command.ExecuteNonQuery();
                    }
                    dbManager.CerrarConexion();
                }
                catch (SqlException exception)
                {
                    Console.WriteLine(exception.Errors);
                    return false;
                }
            }
            return true;
        }
    }
}
