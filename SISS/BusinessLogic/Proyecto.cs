﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class Proyecto
    {
        public int Id { get; set; }
        public String Nombre { get; set; }
        public String Descripcion { get; set; }
        public int NumeroAlumnosMax { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaTerminacion { get; set; }
        public String Estado { get; set; }
    }
}
