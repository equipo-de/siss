﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    class Alumno : Persona
    {
        private String Matricula { get; set; }
        private int HorasRealizadas { get; set; }
        private String Estado { get; set; }
    }
}
